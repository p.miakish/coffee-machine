package com.epam.coffeemachine.beverages;

import java.math.BigDecimal;
import java.util.Objects;

public class CondimentDecorator implements Beverage {
    private final Beverage beverage;
    private final CondimentType condimentType;
    private final double tradeMarkupCoefficient;

    public CondimentDecorator(final Beverage beverage, final CondimentType condimentType, double tradeMarkupCoefficient)
            throws IllegalArgumentException, NullPointerException {
        this.beverage = Objects.requireNonNull(beverage, "A beverage cannot be null");
        this.condimentType = Objects.requireNonNull(condimentType, "A condiment type cannot be null");
        if (tradeMarkupCoefficient > 0.0) {
            this.tradeMarkupCoefficient = tradeMarkupCoefficient;
        } else {
            throw new IllegalArgumentException("Incorrect trade markup coefficient value");
        }
    }

    @Override
    public BigDecimal cost() {
        return beverage.cost().add(condimentType.getPrice().multiply(BigDecimal.valueOf(tradeMarkupCoefficient)));
    }

    @Override
    public String name() {
        return beverage.name() + ", " + condimentType.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CondimentDecorator that = (CondimentDecorator) o;
        return Double.compare(that.tradeMarkupCoefficient, tradeMarkupCoefficient) == 0 &&
                Objects.equals(beverage, that.beverage) &&
                condimentType == that.condimentType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(beverage, condimentType, tradeMarkupCoefficient);
    }

    @Override
    public String toString() {
        return "CondimentDecorator{" +
                "beverage=" + beverage +
                ", condimentType=" + condimentType +
                ", tradeMarkupCoefficient=" + tradeMarkupCoefficient +
                '}';
    }
}
