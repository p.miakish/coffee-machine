package com.epam.coffeemachine.beverages;

import java.math.BigDecimal;

public interface Beverage {
    BigDecimal cost();
    String name();
}
