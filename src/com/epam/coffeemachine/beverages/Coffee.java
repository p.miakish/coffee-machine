package com.epam.coffeemachine.beverages;

import java.math.BigDecimal;
import java.util.Objects;

public class Coffee implements Beverage {
    private final CoffeeType coffeeType;
    private final int sugarAmount;
    private final double tradeMarkupCoefficient;

    public Coffee(CoffeeType coffeeType, int sugarAmount, double tradeMarkupCoefficient) throws
            IllegalArgumentException, NullPointerException {
        this.coffeeType = Objects.requireNonNull(coffeeType, "A type of coffee cannot be null");
        this.sugarAmount = sugarAmount;
        if (tradeMarkupCoefficient > 0.0) {
            this.tradeMarkupCoefficient = tradeMarkupCoefficient;
        } else {
            throw new IllegalArgumentException("Incorrect trade markup coefficient value");
        }
    }

    @Override
    public BigDecimal cost() {
        return coffeeType.getPrice().multiply(BigDecimal.valueOf(tradeMarkupCoefficient));
    }

    @Override
    public String name() {
        return coffeeType.name();
    }

    public CoffeeType getCoffeeType() {
        return coffeeType;
    }

    public int getSugarAmount() {
        return sugarAmount;
    }

    public double getTradeMarkupCoefficient() {
        return tradeMarkupCoefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coffee coffee = (Coffee) o;
        return sugarAmount == coffee.sugarAmount &&
                Double.compare(coffee.tradeMarkupCoefficient, tradeMarkupCoefficient) == 0 &&
                coffeeType == coffee.coffeeType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coffeeType, sugarAmount, tradeMarkupCoefficient);
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "coffeeType=" + coffeeType +
                ", sugarAmount=" + sugarAmount +
                ", tradeMarkupCoefficient=" + tradeMarkupCoefficient +
                '}';
    }
}
