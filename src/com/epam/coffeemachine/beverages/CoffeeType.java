package com.epam.coffeemachine.beverages;

import java.math.BigDecimal;

public enum CoffeeType {
    ESPRESSO(.99),
    AMERICANO(1.19),
    LUNGO(1.39),
    RISTRETTO(1.49);

    private final BigDecimal price;

    CoffeeType(double price) {
        this.price = BigDecimal.valueOf(price);
    }
    public BigDecimal getPrice() {
        return price;
    }

}
