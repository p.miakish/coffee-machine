package com.epam.coffeemachine.beverages;

import java.math.BigDecimal;

public enum CondimentType {
    MILK(.3),
    CREAM(.5),
    CARAMEL(0.35),
    CHOCOLATE(0.6);

    private final BigDecimal price;

    CondimentType(double price) {
        this.price = BigDecimal.valueOf(price);
    }
    public BigDecimal getPrice() {
        return price;
    }
}
