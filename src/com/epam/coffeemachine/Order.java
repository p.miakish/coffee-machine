package com.epam.coffeemachine;

import com.epam.coffeemachine.beverages.CoffeeType;
import com.epam.coffeemachine.beverages.CondimentType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Order {
    private final CoffeeType coffee;
    private final List<CondimentType> condiments = new ArrayList<>();
    private final int sugarAmount;
    private final BigDecimal primeCost;

    public Order(CoffeeType coffee, int sugarAmount, CondimentType ...condiments) throws IllegalArgumentException,
            NullPointerException {
        this.coffee = Objects.requireNonNull(coffee, "Coffee cannot be null");
        if (sugarAmount >= 0) {
            this.sugarAmount = sugarAmount;
        } else {
            throw new IllegalArgumentException("Amount of sugar cannot be a negative number");
        }
        this.condiments.addAll(Arrays.asList(condiments));
        this.primeCost = this.condiments.stream().map(CondimentType::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add)
                .add(coffee.getPrice());
    }

    public CoffeeType getCoffee() {
        return coffee;
    }

    public List<CondimentType> getCondiments() {
        return condiments;
    }

    public int getSugarAmount() {
        return sugarAmount;
    }

    public BigDecimal getPrimeCost() {
        return primeCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return sugarAmount == order.sugarAmount &&
                coffee == order.coffee &&
                Objects.equals(condiments, order.condiments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(coffee, condiments, sugarAmount);
    }

    @Override
    public String toString() {
        return "Order{" +
                "coffee=" + coffee +
                ", condiments=" + condiments +
                ", sugarAmount=" + sugarAmount +
                ", primeCost=" + primeCost +
                '}';
    }
}
