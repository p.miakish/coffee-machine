package com.epam.coffeemachine;

import com.epam.coffeemachine.beverages.*;

public class Test {
    public static void main(String[] args) {
        System.out.println("Create a coffee machine...");
        CoffeeMachine coffeeMachine = new CoffeeMachine(LocationType.UNIVERSITY);
        System.out.println(coffeeMachine.toString());

        System.out.println("Current balance: " + coffeeMachine.getAvailableBalance());
        System.out.println("Increase the balance...");
        coffeeMachine.increaseBalance(20.0);
        System.out.println("The balance is increased");
        System.out.println("Current balance: " + coffeeMachine.getAvailableBalance() + "\n");

        System.out.println("Create an order...");
        Order order = new Order(CoffeeType.ESPRESSO, 2, CondimentType.MILK, CondimentType.MILK,
                CondimentType.CARAMEL, CondimentType.CHOCOLATE);
        coffeeMachine.setOrder(order);
        System.out.println(coffeeMachine.getOrder().toString());
        System.out.println("Order cost: " + coffeeMachine.getOrderCostWithTradeMarkup() + "\n");

        System.out.println("Make coffee...");
        Beverage testCoffee = coffeeMachine.makeCoffee();
        System.out.println(testCoffee.name() + " $" + testCoffee.cost() + "\n");
        System.out.println("Current balance: " + coffeeMachine.getAvailableBalance());
    }
}
