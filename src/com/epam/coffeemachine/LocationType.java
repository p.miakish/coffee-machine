package com.epam.coffeemachine;

import java.math.BigDecimal;

public enum LocationType {
    UNIVERSITY(1.5),
    DORMITORY(1.0),
    HOSPITAL(1.3),
    STREET(2.0);

    private final double coefficient;

    LocationType(double coefficient) {
        this.coefficient = coefficient;
    }
    public double getCoefficient() {
        return coefficient;
    }
}
