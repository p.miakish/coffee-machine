package com.epam.coffeemachine;

import com.epam.coffeemachine.beverages.Beverage;
import com.epam.coffeemachine.beverages.Coffee;
import com.epam.coffeemachine.beverages.CondimentDecorator;
import com.epam.coffeemachine.beverages.CondimentType;
import java.math.BigDecimal;
import java.util.Objects;

public class CoffeeMachine {
    private LocationType location;
    private Order order;
    private BigDecimal availableBalance = BigDecimal.ZERO;
    private BigDecimal income = BigDecimal.ZERO;

    public CoffeeMachine(LocationType location) throws NullPointerException {
        this.location = Objects.requireNonNull(location, "A coffee machine location cannot be null");
    }

    public LocationType getLocation() {
        return location;
    }

    public void setLocation(LocationType location) {
        this.location = Objects.requireNonNull(location, "A coffee machine location cannot be null");
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) throws NullPointerException {
        this.order = Objects.requireNonNull(order, "An order cannot be null");
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void increaseBalance(double sum) throws IllegalArgumentException {
        if (sum > 0) {
            this.availableBalance = this.availableBalance.add(BigDecimal.valueOf(sum));
        } else {
            throw new IllegalArgumentException("The balance cannot be increased, check the sum");
        }
    }

    public BigDecimal getOrderCostWithTradeMarkup() throws UnsupportedOperationException {
        if (order != null) {
            return order.getPrimeCost().multiply(BigDecimal.valueOf(location.getCoefficient()));
        } else {
            throw new UnsupportedOperationException("First you need to place an order");
        }
    }

    public Beverage makeCoffee() throws UnsupportedOperationException {
        BigDecimal orderCost = getOrderCostWithTradeMarkup();
        if (order != null && availableBalance.compareTo(orderCost) >= 0) {
            Beverage beverage = new Coffee(order.getCoffee(), order.getSugarAmount(), location.getCoefficient());
            if (!order.getCondiments().isEmpty()) {
                for (CondimentType condiment : order.getCondiments()) {
                    beverage = new CondimentDecorator(beverage, condiment, location.getCoefficient());
                }
            }
            availableBalance = availableBalance.subtract(beverage.cost());
            income = income.add(beverage.cost());
            return beverage;
        } else {
            throw new UnsupportedOperationException("There is not enough money on the balance to make coffee");
        }
    }

    @Override
    public String toString() {
        return "CoffeeMachine{" +
                "location=" + location +
                ", order=" + order +
                ", availableBalance=" + availableBalance +
                ", income=" + income +
                '}';
    }
}
