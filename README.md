Coffee machine (Epam UpSkill Lab)
-------------------
> This is a training project by P. Miakish

***

#### Functional requirements for a system:

 - the coffee machine should be able to accept an order, increase a balance and make coffee
 - the coffee machine should be able to prepare coffee without condiments and with any number of condiments
 - an exception should be thrown in case of low balance for making coffee
 - a sum equal to an order cost has to be written off from an available balance during order issuing 
    
#### Non-functional requirements for a system:

 - it should be possible to combine any types of condiments at the request of a user, as well as change their list without the need to modify the class of the coffee machine 
 - a final prepared beverage cost should depend on selected components and a location trade markup coefficient 
    
